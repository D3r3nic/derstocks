import { Component, OnInit } from '@angular/core';
import { QuoteService } from '../quote.service';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  storage=window.localStorage
  popularStocks=[
    "AAPL","AMZN","TSLA","INTC","MSFT","GOOG","AMD","GOOGL","BRK.B","BAC","C","NVDA","BRK.A","XOM","T","PFE","GE","CMCSA","WMT","JNJ","CSCO","NOK","TSLA","A","CVX","BABA","VZ","B","DIS","ACB","BEST","QQQ","ADBE","V","F","ZM","JPM","UNH","BIDU","TTM","QCOM","AN","S","AVGO","ORCL","SEE","CHK","I","TGT","RDS.A","GME","GME","GME","GME"]
  // index=[
  //   {exchange:"NAS",name:"Nasdaq Composite"},
  //   {exchange:"OTC",name:"Over-the-counter"},
  //   {exchange:"NYS",name:"The New York Stock Exchange"},
  //   {exchange:"PSE",name:"Philippine Stock Exchange"},
  //   {exchange:"NYS",name:"The New York Stock Exchange"},
  //   {exchange:"NYS",name:"The New York Stock Exchange"},
  //   ];
  popularStocksQuotes=[];
  counter=0;
  freshPage=true;
  more=true;
  collectionTags=[];
  collectionHeader="";
  collectionStocks=[];
  search="";
  searchResult=[];
  portfolio={accountTotal:0,wallet:0,favoriteStocks:[]};
  ownedStocksWithCurrentPrice=[];
  ownedStocks=[];
  buyingPowerPercent="0%";
  gainersAndLosers={gainers:[],losers:[]}
  favoriteMessage="";
  retry=0;
  searchResultErr="";
  userId=this.storage.getItem('userId')

  constructor(private _userService:UserService, private _quoteService:QuoteService,private _router:Router) { }
  ngOnInit() {
    if(this.storage.getItem('logged')!="1"){
      // console.log("please Login")
      this._router.navigate(["/login"])
    }else{
      if(this.freshPage){
        this.arrayOfStockSymbols();
        this.allTags();
        this.getTopGainers();
        this.getTopLosers();
        this.getStocksCurrentValue();
      }
      this.getPortfolioAndTotal();
    }
  }
  // CSS functions
  greenOrRed(a){
    if(a > 0){
      return '#21CE99'
    }
    if(a < 0 ){
      return '#e70000'
    }else{
      return '#000000'
    }
  }
  favoriteOrNot(s){
      // console.log(s)
      
    // this.portfolio.favoriteStocks.forEach(f => {
    //   if(f.symbol==s.symbol){
    //     console.log(s.symbol)
    //     console.log("YES")
    //     return true;
    //   }
    // });
  }
  removeCollectionList(){
    this.collectionStocks=[];
  }
// CSS functions End

  arrayOfStockSymbols(){
    this.freshPage=false;
    let temp
    if(this.popularStocks.length<this.counter+10){
      temp=this.popularStocks.length
      this.more=false;
    }else{
      temp=this.counter+10
    }

    for (let i = this.counter; i < temp ; i++) {
      let tempObservable=this._quoteService.arrayOfStockSymbols(this.popularStocks[i]);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          this.popularStocksQuotes.push(data)
        }
      })
    }
    this.counter=temp
  }

  allTags(){
    let tempObservable=this._quoteService.allTags()
    tempObservable.subscribe((data:any)=>{
      this.collectionTags=data;
    })
  }

  collectionTag(tag){
    this.collectionHeader=tag;
    let tempObservable=this._quoteService.collectionTag(tag)
    tempObservable.subscribe((data:any)=>{
      this.collectionStocks=data
    })
  }

  searchCollection(){
    // Captializing the search
    let str = this.search.split(" ");
    for (var i = 0, x = str.length; i < x; i++) {
        str[i] = str[i][0].toUpperCase() + str[i].substr(1);
    }
    this.search=str.join(" ");
    

    this.searchResult=[]
    this.collectionStocks.forEach(element => {
      if(element.companyName.includes(this.search) || element.symbol.includes(this.search.toUpperCase())){
        this.searchResult.push(element)
      }
    });
    if(this.searchResult.length==0){
      this.searchResultErr="No results found";
    }
  }
  
  getPortfolioAndTotal(){
    let tempObservable=this._userService.getTotal(this.userId)
    tempObservable.subscribe((data:any)=>{
      this.portfolio.wallet=data.wallet;
      this.portfolio.favoriteStocks=data.favoriteStocks;
      this.ownedStocks=data.stocks;
    })
  }

  getStocksCurrentValue(){
    if(this.ownedStocks.length>0){
      // iterate through owned stocks
      this.ownedStocks.forEach(s => {
        let tempObservable=this._quoteService.getPrice(s.stock);
        tempObservable.subscribe((data:any)=>{
          if(!data.err){
            console.log("price",data)
            // get the current data and create new object of live prices
            this.ownedStocksWithCurrentPrice.push({symbol:s.stock,price:data,quantity:s.quantity,totalValue:s.quantity*data})
            // once its done, run calculateTotalCurrentValue.
            if(this.ownedStocksWithCurrentPrice.length==this.ownedStocks.length){
              this.calculateTotalCurrentValue();
            }
          }else{
          }
        })
      });
    }else{
      setTimeout(() => {
        if(this.retry>5){
          if(this.ownedStocks.length<1){
            this.portfolio.accountTotal=this.portfolio.wallet
            this.buyingPowerPercent="100%"
            return;
          }
        }
        this.retry++;
        this.getStocksCurrentValue()
      }, 100);
    }
  }

  calculateTotalCurrentValue(){
    if(this.ownedStocksWithCurrentPrice.length==this.ownedStocks.length){
      this.ownedStocksWithCurrentPrice.forEach(s => {
        this.portfolio.accountTotal=this.portfolio.accountTotal+s.totalValue;
      });
    }else{
      setTimeout(() => {
        this.calculateTotalCurrentValue()
      }, 1000);
    }
    // setting account total value
    
      this.portfolio.accountTotal+=this.portfolio.wallet;
      let percent=(this.portfolio.wallet/this.portfolio.accountTotal)
      percent=percent*100
      this.buyingPowerPercent=String(percent.toFixed(2))+"%"
      // setting each stock`s portfolio Diversity
      this.ownedStocksWithCurrentPrice.forEach(s => {
        s.portfolioDiversity=((s.totalValue/this.portfolio.accountTotal)*100).toFixed(3);
      });
  }

  getTopGainers(){
    let tempObservable=this._quoteService.getTopGainers()
    tempObservable.subscribe((data:any)=>{
      this.gainersAndLosers.gainers=data
    })
  }
  getTopLosers(){
    let tempObservable=this._quoteService.getTopLosers()
    tempObservable.subscribe((data:any)=>{
      this.gainersAndLosers.losers=data
    })
  }

  addToFavorite(s){
    let duplicate=false;
    this.portfolio.favoriteStocks.forEach(element => {
      if(element.symbol===s.symbol){
        duplicate=true
        return;
      }
    })
    if(duplicate){
      this.favoriteMessage=`${s.symbol} is already in your  Watchlist`;
      setTimeout(() => {
        this.favoriteMessage=""
      }, 3000);
    }if(!duplicate){
      let favoriteStock={"symbol":s.symbol,"companyName":s.companyName, userId:this.userId}
      let tempObservable=this._userService.addFavoriteStock(favoriteStock);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          this.ngOnInit();
        }
      })

    }

  }
  removeFavorite(s){
    let favoriteStock={"symbol":s.symbol,"companyName":s.companyName , userId:this.userId}
    let tempObservable=this._userService.addFavoriteStock(favoriteStock);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.ngOnInit();
      }
    })
  }
  // test(){
  //   let tempObservable=this._quoteService.test()
  //   tempObservable.subscribe((data:any)=>{
  //     console.log(data)
  //   })
  // }

  // getStocks(event:any){
  //   console.log(event.target.value)
  //   let tempObservable=this._quoteService.getIndexSymbols(event.target.value)
  //   tempObservable.subscribe((data:any)=>{
  //     this.stocks=data
  //     console.log(data)
  //   })
  // }

  // getIndex(){
  //   let tempObservable=this._quoteService.getIndex()
  //   tempObservable.subscribe((data:any)=>{
  //       this.index=data
  //       console.log(data)
  //   })
  // }

  
}

// let pivot=[]
//       let counter = 0
//       console.log(this.history)
//       data.forEach(item=>{
//         pivot.push(new Array(item.stock,item.price,item.quantity))
//       })


// / let pivot=[]
      // let counter = 0
      // console.log(this.history)
      // data.forEach(item=>{
      
      //   let added=false
      //   if(counter==0){
      //     pivot.push(new Array(item.stock,item.price,item.quantity))
      //     counter++
      //   }
      //   else{
      //     for(let i=0;i<pivot.length;i++){
      //       if(item.stock==pivot[i][0]){
      //         pivot[i][1]+=item.price
      //         //get the average price
      //         pivot[i][1]/=2
      //         pivot[i][2]+=item.quantity
      //         added=true
      //         return
      //       }
      //     }
      //     if (!added){
      //       pivot.push(new Array(item.stock,item.price,item.quantity))
      //     }
      //   }
      // })
      // console.log(pivot)
      // exports an pivot array with the 4th index as the avrage number to divide by the price.
        
        // 0: {_id: "5cca2e8affc8761a49e5fc2f", stock: "AAPL", price: 210.52, quantity: 1, user: "5cca2e66ffc8761a49e5fc2e"}
        // 1: {_id: "5cca2e9cffc8761a49e5fc30", stock: "AAPL", price: 210.52, quantity: 1000, user: "5cca2e66ffc8761a49e5fc2e"}
        // 2: {_id: "5cca39bee884bb1aaf8ab446", stock: "MSFT", price: 127.88, quantity: 10, user: "5cca2e66ffc8761a49e5fc2e"}
        // 3: {_id: "5cca39f27f09661ad53c84b3", stock: "INTC", price: 50.76, quantity: 10, user: "5cca2e66ffc8761a49e5fc2e"}
        // 4: {_id: "5cca4d21b677ae1e57e001c6", stock: "AAPL", price: 210.52, quantity: 10, user: "5cca2e66ffc8761a49e5fc2e"}
        // 5: {_id: "5cca4d29b677ae1e57e001c7", stock: "INTC", price: 50.76, quantity: 10, user: "5cca2e66ffc8761a49e5fc2e"}
        // 6: {_id: "5cca4d2fb677ae1e57e001c8", stock: "GOOG", price: 1168.08, quantity: 20, user: "5cca2e66ffc8761a49e5fc2e"}
        
        // to
        
        // 0: (3) ["AAPL", 210.52, 1011]
        // 1: (3) ["MSFT", 127.88, 10]
        // 2: (3) ["INTC", 50.76, 20]
        // 3: (3) ["GOOG", 1168.08, 20]


