import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { StockQuoteComponent } from './stock-quote/stock-quote.component';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './profile/account/account.component';
import { BankingComponent } from './profile/banking/banking.component';
import { HistoryComponent } from './profile/history/history.component';
import { SettingsComponent } from './profile/settings/settings.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  {path: 'home',component:HomeComponent},
  {path: 'stocks/:stock',component:StockQuoteComponent},
  {path: 'registration',component:LoginRegistrationComponent},
  {path: 'login',component:LoginComponent},
  {path: 'profile', component:ProfileComponent,children:[
    // {path: 'banking',component:BankingComponent},
    {path: 'history',component:HistoryComponent},
    // {path: 'settings',component:SettingsComponent},
    {path: '',pathMatch:'full',component:BankingComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
