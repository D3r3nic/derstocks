import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user={}
  storage=window.localStorage
  userId=this.storage.getItem('userId')
  constructor(private _router:Router,private _userService:UserService) { }

  ngOnInit() {
    let tempObservable= this._userService.getUser(this.userId);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.user=data
      }
    })
  }

}
