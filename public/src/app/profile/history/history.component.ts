import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { QuoteService } from 'src/app/quote.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(private _userService:UserService, private _quoteService:QuoteService,private _router:Router) { }

  buyHistory=[]
  sellHistory=[]
  storage=window.localStorage
  userId=this.storage.getItem('userId')

  ngOnInit() {
    this.getBuyHistory()
    this.getSellHistory()
  }

  getBuyHistory(){
    let tempObservable=this._userService.getBuy(this.userId);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.buyHistory=data.reverse();
      }
    })
  }

  getSellHistory(){
    let tempObservable=this._userService.getSell(this.userId);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.sellHistory=data.reverse();
      }
    })
  }
}
