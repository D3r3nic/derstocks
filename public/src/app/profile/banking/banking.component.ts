import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/user.service';
import { _sanitizeHtml } from '@angular/core/src/sanitization/html_sanitizer';

@Component({
  selector: 'app-banking',
  templateUrl: './banking.component.html',
  styleUrls: ['./banking.component.css']
})
export class BankingComponent implements OnInit {
  valid=false;
  depositValid=false
  newAccount={fullName:"John M. Doe" , email:"john@example.com" ,address:"542 W. 15th Street" ,city:"New York" ,state:"NY" ,zip:"10001" ,cardName:"John More Doe" ,nickName:"Chase Freedom" ,expYear:"", creditCard:"1111-2222-3333-4444" , cvv:"123" , userId:""};
  message="";
  successMessage="";
  depositMessage="";
  user={bankAccounts:[]};
  deposit={amount:0,cardName:"",userId:""};
  bankToRemoveName={nickName:"" ,userId:""};
  transactions=[]
  storage=window.localStorage
  userId=this.storage.getItem('userId')

  constructor(private _userService:UserService) { }

  ngOnInit() {
    // console.log(this.storage.getItem('userId'))
    this.resetFields();
    this.getUser();
    this.getTransactions();
  }

  depositCancel(){
    this.depositValid=false;
  }
  invalid(){
    this.valid=false;
  }
  bankAcctRmovePmtNo(){
    this.bankToRemoveName={nickName:"" ,userId:""};
  }
  bankToRemove(a){
    this.bankToRemoveName.nickName=a;
  }
  resetFields(){
    this.message="";
    this.valid=false;
    this.deposit={amount:0,cardName:"",userId:""};
    this.newAccount={fullName:"John M. Doe" , email:"john@example.com" ,address:"542 W. 15th Street" ,city:"New York" ,state:"NY" ,zip:"10001" ,cardName:"John More Doe" ,nickName:"Chase Freedom" ,expYear:"", creditCard:"1111-2222-3333-4444" , cvv:"123" ,userId:""};
    setTimeout(()=>{
      this.successMessage=""
    }, 10000);
  }



  getUser(){
    let tempObservable= this._userService.getUser(this.userId);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        console.log(data)
        this.deposit.cardName=data.bankAccounts[0]
        this.user=data;
      }
    })
  }

  getTransactions(){
    let tempObservable= this._userService.getTransactions(this.userId);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.transactions=data.reverse();
        console.log(data)
      }else{
        console.log(data.err)
      }
    })

  }


  depositReview(){
    this.depositMessage="";
    // if(this.deposit.amount<=0){
    //   this.depositMessage="Amount must be greater or equal to $ 0.01 "
    //   this.depositValid=false;
    // }
    if(this.deposit.amount>=0.01){
      this.depositValid=true;
    }else{
      this.depositMessage="Please fill both fields"
      this.depositValid=false;
    }
  }
  
  
  bankAcctRmovePmtYes(){
    this.bankToRemoveName.userId=this.userId;
    let tempObservable=this._userService.deleteBankAccount(this.bankToRemoveName);
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.ngOnInit()
      }else{
        console.log(data.err)
      }
    })
  }



  reviewPaymentMethod(){
    let acc=this.newAccount
    if(acc.fullName.length>1 && acc.address.length>1 && acc.city.length>1 && acc.state.length>1 && acc.zip.length>1 && acc.cardName.length>1 && acc.creditCard.length>1 && acc.nickName.length>1 && acc.cvv.length>1){
      this.message="Information accepted, please submit"
      this.valid=true
      return
    }if(acc.creditCard.length<15){
      this.message="Wrong card number"
      return
    }else{
      this.message="Please fill the required fields"
    }
  }

  addAccount(){
    this.newAccount.userId=this.userId
    let tempObservable=this._userService.addBankAccount(this.newAccount)
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.ngOnInit()
      }else{
        this.message=data.err
      }
    })
  }

  depositFunds(){
    this.deposit.userId=this.userId
    if(this.deposit.amount>0 && this.deposit.cardName.length>0){
      let tempObservable=this._userService.depositFunds(this.deposit);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          this.successMessage=`Successfully deposited $${this.deposit.amount} to SproutVestment`;
          this.depositCancel()
          this.ngOnInit()
          
        }else{
          console.log(data.err)
        }
      })
    }else{
      this.message="Please fill the fields with valid information"
    }
  }


}
