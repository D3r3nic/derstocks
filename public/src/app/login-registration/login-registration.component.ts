import { Component, OnInit } from '@angular/core';
import { QuoteService } from '../quote.service';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-registration',
  templateUrl: './login-registration.component.html',
  styleUrls: ['./login-registration.component.css']
})
export class LoginRegistrationComponent implements OnInit {
  newUser={ firstName:"",lastName:"",email:"",password:""};
  confirmation="";
  message=""
  error=""
  storage=window.localStorage
  constructor(private _quoteService:QuoteService, private _userService:UserService,private _router:Router) { }

  ngOnInit() {
  }

  createUser(){
    this.error=""
    if(this.newUser.firstName.length>1 && this.newUser.lastName.length>1 && this.newUser.email.length>5 && this.confirmation.length>0 && this.newUser.password===this.confirmation){
      this.newUser.email=this.newUser.email.toLowerCase();
      let tempObservable=this._userService.createUser(this.newUser);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){

          if(data.message){
            this.message=data.message;
          }else if(data.success){
            this.storage.setItem("logged","1")
            for(let i in data.userInfo){
            this.storage.setItem(i,data.userInfo[i])
            }
            this._router.navigate(['/home'])
          }
        }else{
          this.error=data.err
        }
      })

    }else{
      this.error="Please enter the required fields mentioned above."
    }
  }

}
