import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { QuoteService } from '../quote.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-stock-quote',
  templateUrl: './stock-quote.component.html',
  styleUrls: ['./stock-quote.component.css']
})
export class StockQuoteComponent implements OnInit {
  freshPage=true;
  message="";
  err="";
  view=0;
  validBuyOrder=false;
  validSellOrder=false;
  pageColor="";
  portfolio={
    equity:0,
    quantity:0,
    favoriteStocks:[],
    wallet:0
  };

  stock={
    quote:{
      symbol:"",
      changePercent:0,
      iexRealtimePrice:0,
      close:0
    },
    about:{},
    news:[],
    earnings:[],
    chart:[],
    chartboundary:0
  };

  long={
    stock:"",
    price:0,
    quantity:0,
    userId:""
  };
  
  sell={
    stock:"",
    price:0,
    quantity:0,
    userId:""
  };
  maxAmountBuyPower=0;
  colorRetry=0;
  storage=window.localStorage
  userId=this.storage.getItem('userId')

  constructor(private _quoteService:QuoteService,private _userService:UserService ,private _route:ActivatedRoute,) { }

 

  buyView(){
    if(this.view!==0){
      this.view=0
    }
  }
  sellView(){
    if(this.view!==1){
      this.view=1
    }
  }

  editBuy(){
    this.validBuyOrder=!this.validBuyOrder
  }
  editSell(){
    this.validSellOrder=!this.validSellOrder
  }

  reviewBuyOrder(input){
  this.message=""
    if(this.long.quantity>0){
      this.validBuyOrder=true
    }else{
      this.message=`Shares quantity must be 1 or more`;
    }
  }

  reviewSellOrder(input){
  this.message=""
    if(this.sell.quantity>0){
      this.validSellOrder=true
    }else{
      this.message=`Shares quantity must be 1 or more`;
    }
  }
  quantityMathFloor(){
    setTimeout(() => {
      if(this.stock.quote.close==null){
        this.maxAmountBuyPower=Math.floor(this.portfolio.wallet/this.stock.quote.iexRealtimePrice)
      }
      if(this.stock.quote.close!=null){
        this.maxAmountBuyPower=Math.floor(this.portfolio.wallet/this.stock.quote.close)
      }
    }, 1000);
  }

  ngOnInit() {
    if(this.freshPage){
      this.freshPageLoad()
    }
    this.refreshPrice();
    this.greenOrRed();
    this.resfreshVariables();
    this.quantityMathFloor();
    if (today >= 5 && today <= 13) {
      var today = new Date().getHours();
      setInterval(()=>{
        this.refreshPrice()
      }, 10000);
    } 
  }
  freshPageLoad(){
    this.freshPage=false;
    this.getAbout();
    this.getNews();
    this.getEarnings();
  }

  resfreshVariables(){
    this.long.quantity=0
    this.sell.quantity=0
    this.validSellOrder=false
    this.validBuyOrder=false
    this.getEquityInfo();
    this.getTotalAccount();
    this.getTotalAccount();
  }
  
  refreshPrice(){
    this._route.params.subscribe((params:Params)=>{
      let tempObservable=this._quoteService.getQuote(params['stock']);
      tempObservable.subscribe((data:any)=>{
        if(!data.error){
          console.log(data)
          this.stock.quote=data;
        }
      })
    })
  }

  getAbout(){
    this._route.params.subscribe((params:Params)=>{
      let tempObservable=this._quoteService.getAbout(params['stock']);
        tempObservable.subscribe((data:any)=>{
          if(!data.error){
            this.stock.about=data;
          }
        })
      })
  }

  getNews(){
    this._route.params.subscribe((params:Params)=>{
      let tempObservable3=this._quoteService.getNews(params['stock']);
        tempObservable3.subscribe((data:any)=>{
          if(!data.error){
            this.stock.news=data;
          }
        })
    })
  }

  getEarnings(){
    this._route.params.subscribe((params:Params)=>{
      let tempObservable4=this._quoteService.getEarnings(params['stock']);
        tempObservable4.subscribe((data:any)=>{
          if(!data.error){
            this.stock.earnings=data.earnings.reverse()
            this.earningsChart()
          }
        })
    })
  }
  earningsChart(){
    let max=this.stock.earnings[0].actualEPS;
    this.stock.earnings.forEach(element => {
      let eps=element.actualEPS;
      // in case eps is negative and larger than regular eps, count the negative 
      if(eps<0){
        eps=eps*-1;
      }
      if(eps>max){
        max=eps;
      }
    });
    this.stock.chartboundary=max+max/4;
    this.stock.earnings.forEach(element => {
      if(element.actualEPS>0){
        // get the ratio of actual EPS to chart boundary, 
        // multiply to 100(percent), 
        // to 2 decimal places, 
        // convert to string and add % sign
        let epsPercent=(element.actualEPS/this.stock.chartboundary*100).toFixed(2).toString()+"%";
        this.stock.chart.push({"positiveHeight":epsPercent})
      }if(element.actualEPS<0){
        let eps=element.actualEPS*-1;
        let epsPercent=(eps/this.stock.chartboundary*100).toFixed(2).toString()+"%";
        this.stock.chart.push({"negativeHeight":epsPercent})
      }
    });
  }


  buyStock(){
    if(this.validBuyOrder){
      this.refreshPrice();
      if(this.stock.quote.close==null){
        this.long.price=this.stock.quote.iexRealtimePrice;
      }
      if(this.stock.quote.close!=null){
        this.long.price=this.stock.quote.close;
      }
      this.long.stock=this.stock.quote.symbol;
      this.long.userId=this.userId;
      let tempObservable=this._userService.buyStocks(this.long);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          this.message=data.message;
          setTimeout(() => {
            this.message=""
          }, 10000);
          this.ngOnInit();
        }
      })

    }if(this.long.quantity>0){
      this.message=`Please press review`;
    }
    else{
      this.message=`Shares quantity must be 1 or more`;
    }
  }

  sellStock(){
    if(this.validSellOrder){
      this.refreshPrice()
      if(this.stock.quote.close==null){
        this.sell.price=this.stock.quote.iexRealtimePrice;
      }
      if(this.stock.quote.close!=null){
        this.sell.price=this.stock.quote.close;
      }
      this.sell.stock=this.stock.quote.symbol
      this.sell.userId=this.userId;
      let tempObservable=this._userService.sellStocks(this.sell);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          this.message=data.message;
          setTimeout(() => {
            this.message=""
          }, 10000);
          this.ngOnInit();
        }
      })
    }if(this.sell.quantity>0){
      this.message=`Please press review`;
    }
    else{
      this.message=`Shares quantity must be 1 or more`;
    }
  }

  getEquityInfo(){
    this._route.params.subscribe((params:Params)=>{
      let tempObservable=this._userService.getEquity(params['stock'],this.userId)
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          if(data.message){
            this.portfolio.equity=0;
            this.portfolio.quantity=0;
            return
          }if(data.equity){
            this.portfolio.equity=data.equity
            this.portfolio.quantity=data.quantity
            return
          }
        }else{
          this.err=data.err
        }
      })
      
    })
  }

  getTotalAccount(){
    let tempObservable=this._userService.getTotal(this.userId)
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        this.portfolio.wallet=data.wallet
      }else{
        this.err=data.err
      }
    })
  }

  // CSS Commands
  greenOrRed(){
    setTimeout(() => {
      if(this.stock.quote.changePercent > 0){
        this.pageColor='#21CE99'
        return
      }if(this.stock.quote.changePercent < 0 ){
        this.pageColor='#e70000'
        return
      }
      else{
        this.colorRetry++
        if(this.colorRetry==10){
          this.pageColor='#ffffff'
          return
        }else{
          this.greenOrRed();
        }

      }
    }, 300);
  }
  // CSS Commands END

}