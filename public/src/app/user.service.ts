import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http:HttpClient) { }

  createUser(newUser){
    return this._http.post('/stocks/user', newUser);
  }

  loginUser(user){
    return this._http.post('/stocks/userlogin',user)
  }
  logOut(){
    console.log("services");
    return this._http.get('/stocks/userLogOut')
  }
  buyStocks(info){
    return this._http.post('/stocks/buy', info)
  }
  
  sellStocks(info){
    return this._http.post('/stocks/sell', info)
  }

  getBuy(id){
    return this._http.get(`/stocks/history/buy/${id}`)
  }

  getSell(id){
    return this._http.get(`/stocks/history/sell/${id}`)
  }

  getEquity(info,id){
    return this._http.get(`/stocks/equity/${info}/${id}`)
  }

  getTotal(id){
    return this._http.get(`/stocks/totalAccount/${id}`)
  }

  getUser(id){
    return this._http.get(`/stocks/user/${id}`)
  }

  addBankAccount(info){
    return this._http.post('/stocks/addBankAccount',info)
  }

  deleteBankAccount(bank){
    return this._http.post('/stocks/delBankAccount',bank)
  }

  depositFunds(deposit){
    return this._http.post('/stocks/depositFunds',deposit)
  }

  getTransactions(id){
    return this._http.get(`/stocks/transactions/${id}`)
  }

  addFavoriteStock(favorite){
    return this._http.post('/stocks/favorite',favorite)
  }
}
