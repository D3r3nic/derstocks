import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user={email:"",password:""}
  message=""
  error=""
  storage=window.localStorage
  constructor(private _userService:UserService, private _router:Router) { }

  ngOnInit() { 
    
  }

  login(){
    if(this.user.email.length<1 || this.user.password.length<1){
      this.message="Please enter both fields"
    }else{
      this.user.email=this.user.email.toLowerCase();
      let tempObservable=this._userService.loginUser(this.user);
      tempObservable.subscribe((data:any)=>{
        if(!data.err){
          if(data.message){
            this.message=data.message
          }else if(data.LoggedIn){
            this.message, this.error=""
            this.storage.setItem("logged","1")
            for(let i in data.userInfo){
              this.storage.setItem(i,data.userInfo[i])
            }
            this._router.navigate(['/home'])
          }
        }else{
          this.error=data.err
        }
      })
    }
  }

}
