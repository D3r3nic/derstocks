import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { QuoteService } from './quote.service';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StockQuoteComponent } from './stock-quote/stock-quote.component';
import { HomeComponent } from './home/home.component';
import { LoginRegistrationComponent } from './login-registration/login-registration.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './profile/account/account.component';
import { BankingComponent } from './profile/banking/banking.component';
import { HistoryComponent } from './profile/history/history.component';
import { SettingsComponent } from './profile/settings/settings.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    StockQuoteComponent,
    HomeComponent,
    LoginRegistrationComponent,
    LoginComponent,
    ProfileComponent,
    AccountComponent,
    BankingComponent,
    HistoryComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [QuoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
