import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private _userServices:UserService ,private _router:Router){}

  storage=window.localStorage
  

  ngOnInit(){
    if(this.storage.getItem('logged')!="1"){
      this._router.navigate(["/login"])
    }else{
      this._router.navigate(['/home'])
    }
  }
  logout(){
    let tempObservable=this._userServices.logOut();
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
        // console.log(data);
        this.storage.clear()
      }else{
        console.log(data.err);
      }
    })
  }

  // @HostListener('window:beforeunload')
  // async ngOnDestroy() {
  //   this.storage.clear()
  //   await this.logout();
  //   await this.ngOnInit();
  // }
  
  logOut(){
    let tempObservable=this._userServices.logOut();
    tempObservable.subscribe((data:any)=>{
      if(!data.err){
      }else{
        console.log(data.err);
      }
    })
  }
  
}