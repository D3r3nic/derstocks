import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})

export class QuoteService {
  token="pk_26ff933237f3438d9ed9cbab4d2587e8";

  constructor(private _http :HttpClient) {}

  getQuote(symbol){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/${symbol}/quote?token=${this.token}`)
  }
  getAbout(symbol){
     return this._http.get(`https://cloud.iexapis.com/beta/stock/${symbol}/company?token=${this.token}`)
  }
  getNews(symbol){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/${symbol}/news/last/4?token=${this.token}`)
  }
  getEarnings(symbol){
    return this._http.get(`https://cloud.iexapis.com/beta//stock/${symbol}/earnings/4/1?token=${this.token}`)
  }
  getIndexSymbols(index){
    return this._http.get(`https://cloud.iexapis.com/beta/ref-data/exchange/${index}/symbols?token=${this.token}`)
  }
  getIndex(){
    return this._http.get(`https://cloud.iexapis.com/beta/ref-data/market/us/exchanges?token=${this.token}`)
  }
  getMostActive(){
    return this._http.get(`https://cloud.iexapis.com/beta//stock/market/list/mostactive?token=${this.token}`)
  }
  getTopGainers(){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/market/list/gainers?token=${this.token}`)
  }
  getTopLosers(){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/market/list/losers?token=${this.token}`)
  }
  arrayOfStockSymbols(array){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/${array}/quote?token=${this.token}`)
  }
  allTags(){
    return this._http.get(`https://cloud.iexapis.com/beta/ref-data/tags?token=${this.token}`)
  }
  collectionTag(tag){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/market/collection/tag?collectionName=${tag}&token=${this.token}`)
  }
  getPrice(symbol){
    return this._http.get(`https://cloud.iexapis.com/beta/stock/${symbol}/price?token=${this.token}`)
  }
  
}

// /stock/{symbol}/earnings/{last}/{field}
// /stock/{symbol}/news
// GET /stock/{symbol}/price

// GET /stock/{symbol}/company




// GET /stock/{symbol}/largest-trades

// GET /stock/market/upcoming-ipos
// GET /stock/market/today-ipos

// GET /stock/market/list/{list-type}
// /stock/market/list/mostactive
// /stock/market/list/gainers
// /stock/market/list/losers
// default10




// GET /stock/market/volume
