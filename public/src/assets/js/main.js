$( document ).ready(function() {
    $(document).on('click','.tabBox',function(){
        $(this).addClass('active').siblings().removeClass('active')
    });


    $(document).on('click','#popUp,.noAcc',function(){
        $('.paymentForm,.bg').addClass('pop');
    });
    $(document).on('click','.formClose,.submitForm button',function(){
        $('.paymentForm,.bg').removeClass('pop');
    });
    

    $(document).on('click','#removeBank',function(){
        $('.removePop,.removeBg').addClass('pop');
    });
    $(document).on('click','.promtButton',function(){
        $('.removePop,.removeBg').removeClass('pop');
    });


    $(document).on('keydown', 'input[pattern]', function(e){
        var input = $(this);
        var oldVal = input.val();
        var regex = new RegExp(input.attr('pattern'), 'g');
      
        setTimeout(function(){
          var newVal = input.val();
          if(!regex.test(newVal)){
            input.val(oldVal); 
          }
        }, 0);
    });


    $(document).on('click','#depositReviewButton',function(){
        $('.depositReviewNote').addClass('depositOpenAnimation');
    });
    $(document).on('click','#depositReviewCloseButton,#deposit',function(){
        $('.depositReviewNote').removeClass('depositOpenAnimation');
    });
    $(document).on('click','.invalid',function(){
        $('.depositReviewNote').removeClass('depositOpenAnimation');
    });


    $(document).on('click','.tag',function(){
        $('.popUpBg').addClass('pop');
    });
    $(document).on('click','#closePop',function(){
        $('.popUpBg').removeClass('pop');
    });

    // $(".portfolioBar").hover(function(){
    //     console.log("hello")
    //     $('.buyingPower').removeClass('fill');
    // });
    
    $(document).on('click','#collectionOpenDrawer',function(){
        $('.collectionTags').addClass('initial')
        $('#collectionOpenDrawer').addClass('displayNone');
        $('#collectionCloseDrawer').removeClass('displayNone');
    });

    $(document).on('click','#collectionCloseDrawer',function(){
        $('.collectionTags').removeClass('initial')
        $('#collectionOpenDrawer').removeClass('displayNone');
        $('#collectionCloseDrawer').addClass('displayNone');
    });


    $(document).on('click','#gainerOpenDrawer',function(){
        $('.topGainers').addClass('initialGainersLosers')
        $('#gainerOpenDrawer').addClass('displayNone');
        $('#gainerCloseDrawer').removeClass('displayNone');
    });
    $(document).on('click','#gainerCloseDrawer',function(){
        $('.topGainers').removeClass('initialGainersLosers')
        $('#gainerOpenDrawer').removeClass('displayNone');
        $('#gainerCloseDrawer').addClass('displayNone');
    });


    $(document).on('click','#loserOpenDrawer',function(){
        $('.topLosers').addClass('initialGainersLosers')
        $('#loserOpenDrawer').addClass('displayNone');
        $('#loserCloseDrawer').removeClass('displayNone');
    });
    
    $(document).on('click','#loserCloseDrawer',function(){
        $('.topLosers').removeClass('initialGainersLosers')
        $('#loserOpenDrawer').removeClass('displayNone');
        $('#loserCloseDrawer').addClass('displayNone');
    });
    
    $(document).on('click','#phoneBuyButton,#phoneSellButton',function(){
        $('.buyOrSell').addClass('showBuyorSell');
    });
    

});