const users = require('../controllers/users.js');
path=require("path")
module.exports = function(app){
  app.post('/stocks/user', users.createUser);
  app.post('/stocks/userlogin', users.loginUser);
  app.get('/stocks/userLogOut', users.logoutUser);
  app.get('/stocks/user/:id', users.getUser);

  app.post('/stocks/buy',users.buyStocks);
  app.post('/stocks/sell',users.sellStocks);
  app.get('/stocks/history/buy/:id',users.getBuy);
  app.get('/stocks/history/sell/:id',users.getSell);
  app.get('/stocks/equity/:info/:id',users.getEquity);
  app.get('/stocks/totalAccount/:id',users.getTotalAccountValue);
  app.post('/stocks/addBankAccount',users.createBankAccount);
  app.post('/stocks/delBankAccount',users.deleteBankAccount);
  app.post('/stocks/depositFunds',users.depositFunds);
  app.get('/stocks/transactions/:id',users.getTransactions);
  app.post('/stocks/favorite',users.addFavorite);

  app.all("*", (req,res,next) => {
    res.sendFile(path.resolve("./public/dist/public/index.html"))
  });
} 