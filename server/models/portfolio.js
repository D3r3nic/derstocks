var mongoose=require('mongoose');
Schema=mongoose.Schema;
UserSchema=require('./user.js')


var PortfolioSchema = new mongoose.Schema({
  stock: {type: String, required: true, minlength: 1 },
  quantity: {type: Number, required:true},
  equity: {type:Number},
  user:{
    type: Schema.Types.ObjectId,
    ref: "User"
    }
  }, {timestamps: true})
mongoose.model('Portfolio', PortfolioSchema); 