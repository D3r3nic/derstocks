var mongoose=require('mongoose');
Schema=mongoose.Schema;
UserSchema=require('./user.js')


var BuySchema = new mongoose.Schema({
  stock: {type: String, required: true, minlength: 1 },
  price: {type: Number, required: true},
  quantity: {type: Number,required:true, min:1},
  user:{
    type: Schema.Types.ObjectId,
    ref: "User"
      }
  }, {timestamps: true})
mongoose.model('Buy', BuySchema); 