var mongoose=require('mongoose');
Schema=mongoose.Schema;
UserSchema=require('./user.js')


var TransactionSchema = new mongoose.Schema({
    account: {type: String, required: true, minlength: 1 },
    transferAmount: {type: Number, required:true},
    user:{
        type: Schema.Types.ObjectId,
        ref: "User"
        }
  }, {timestamps: true})
mongoose.model('Transaction', TransactionSchema); 