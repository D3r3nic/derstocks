var mongoose=require('mongoose');
Schema=mongoose.Schema;
BuySchema=require('./buy.js')
BuySchema=require('./buy.js')


var UserSchema = new mongoose.Schema({
  firstName: {type: String, required: true, minlength: 2 },
  lastName: {type: String, required: true, minlength: 2 },
  password: {type: String, required: true, minlength: 2 },
  email: {type: String, required: true },
  bankAccounts: {type: Array},
  favoriteStocks: {type: Array},
  wallet: {type: Number,default:100000}
  }, {timestamps: true})
mongoose.model('User', UserSchema); 