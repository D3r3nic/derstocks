var mongoose = require('mongoose');
var User = mongoose.model('User');
var Buy= mongoose.model('Buy')
var Sell= mongoose.model('Sell')
var Portfolio=mongoose.model('Portfolio')
var Transaction=mongoose.model('Transaction')

// 
// var active={
// };
module.exports = {
	createUser:function (req, res) {
		User.findOne({email:(req.body.email).toLowerCase()}, function (err, user){
			// if not error then 
			if(!err){
				// search for the user and
				if(user){
					// if user exists
					res.json({message:"There is already a user registered with that Email"})
				}else{
					// doesnt exist , so create user
					User.create(req.body,function(err,user){
						if(!err){
							res.json({
								"success":true,
								"userInfo":{
									"firstName":user.firstName,
									"lastName":user.lastName,
									"userId":user.id
								}
							})
						}else{
							res.json(err)
						}
					})
				}
			}else{
				res.json(err)
			}
		})
	},

	loginUser:function (req,res){
		// search for user by email

		User.findOne({email:(req.body.email).toLowerCase()},function(err,user){
			// if there are no errors
			if(!err){
				// if user exists
				if(user){
					// console.log(user)
					//check if the password is correct
					if(req.body.password==user.password){
						res.json({
							"LoggedIn":true,
							"userInfo":{
								"firstName":user.firstName,
								"lastName":user.lastName,
								"userId":user.id
							}
						})
					}else{
						res.json({message:"Invalid Password"})
					}
				}else{
					res.json({message:" The entered email is not associated with an account."})
				}
			}else{
			// if has error ,respond with error
				res.json(err)
			}
		})
	},
	logoutUser:function(req,res){
		// conseole.log("backeend");
		res.json({message:"User Logged Out"})
	},

	getUser:function(req,res){
		User.findById({_id:req.params.id},function(err,user){
			// console.log(user)
			if(!err){
				res.json({
					firstName:user.firstName,
					lastName:user.lastName,
					wallet:user.wallet,
					bankAccounts:user.bankAccounts
				})
			}else{
				res.json(err)
			}
		})
	},


	buyStocks:function(req,res){
		// find the user
		User.findOne({_id:req.body.userId},function(err,user){
			if(!err){
				// if no errors, create variables
				stockName=req.body.stock
				stockPrice=req.body.price
				buyQuantity=req.body.quantity
				totalCost=buyQuantity*stockPrice

				// check to see if wallet has enough funds to purchase
				if(user.wallet<totalCost){

					// if doesnt, return the funds needed and the allowerd amount of stocks to buy with the current funds.
					allowed=Math.floor(user.wallet/stockPrice)
					difference=(totalCost-user.wallet).toFixed(2)
					res.json({"message":`You don't have enough buying power. You can purchase ${allowed} of ${stockName} or deposit ${difference}$`})

				// elseif there is enough money
				}else{
					// create a buy history 
					Buy.create(req.body,function(err,buy){
						if(!err){
							buy.user=req.body.userId
							// find the the record of user with the stock 
							Portfolio.findOne({user:req.body.userId,stock:stockName},function(err,portfolio){
								if(!err){
									if(portfolio){
										portfolio.quantity+=buyQuantity
										portfolio.equity+=totalCost
										portfolio.save()
										response()
									}else{
										// if its a first time buy then create a record
										Portfolio.create(req.body,function(err,portfolio){
											if(!err){
												portfolio.equity=totalCost
												portfolio.user=req.body.userId
												portfolio.save()
												response()
											}else{
												res.json(err)
											}
										})
									}
								}else{
									res.json(err)
								}
								function response(){
									// there are no errors. save all the records and respond
										user.wallet-=totalCost
										user.save()
										buy.save()
										res.json({"message":`Successfully purchased ${buyQuantity} ${stockName} at ${stockPrice}$ each`})
								}
							})
						}else{
							// but if theres error delete the buying record
							res.json(err)
						}
					})
				}
			}else{
				res.json(err)
			}
		})
	},

	getBuy:function(req,res){
		console.log("buy", req.params.id)
		Buy.find({user:req.params.id},function(err,data){
			console.log("buy", data)
			if(!err){
				res.json(data)
			}else(
				res.json(err)
			)
		})
	},

	
	sellStocks:function(req,res){
		User.findOne({_id:req.body.userId},function(err,user){
			if(!err){
				stockName=req.body.stock
				stockPrice=req.body.price
				sellQuantity=req.body.quantity
				totalCredit=sellQuantity*stockPrice
				Portfolio.findOne({user:req.body.userId,stock:stockName},function(err,portfolio){
					if(!err){
						if(portfolio){
							if(portfolio.quantity>=sellQuantity){
								Sell.create(req.body,function(err,sell){
									sell.user=req.body.userId
									if(!err){
										if(portfolio.quantity==sellQuantity){
											Portfolio.deleteOne({_id:portfolio.id},function(err){
												if(!err){
													user.wallet+=totalCredit
													user.save()
													sell.save()
													res.json({"message":`Successfully sold ${sellQuantity} ${stockName} at ${stockPrice}$ each`})
												}else{
													res.json(err)
												}
											})
										}else{
											user.wallet+=totalCredit
											portfolio.quantity-=req.body.quantity
											portfolio.equity-=totalCredit
											portfolio.save()
											user.save()
											sell.save()
											res.json({"message":`Successfully sold ${sellQuantity} ${stockName} at ${stockPrice}$ each`})
										}
									}else{
										sell.remove()
										res.json(err)
									}
								})
							}else{
								if(portfolio.quantity==0){
									res.json({message:`You dont currently own any of ${portfolio.stock}`})

								}else{
									res.json({message:`Cannot sell more stocks than there are in the account. Max allowed is: ${portfolio.quantity} of ${portfolio.stock}`})
								}
							}
						}else{
							res.json({message:`You dont have any ${stockName} to sell`})
						}
					}else{
						res.json(err)
					}
				})
				
			}else{
				res.json(err)
			}
		})
	},

	getSell:function(req,res){
		console.log("sell", req.params.id)
		Sell.find({user:req.params.id},function(err,data){
			if(!err){
				console.log("sell", data)
				res.json(data)
			}else(
				res.json(err)
			)
		})
	},

	getTotalAccountValue:function(req,res){
		User.findById({_id:req.params.id},function(err,user){
			if(!err){
				Portfolio.find({user:req.params.id},function(err,portfolio){
					if(!err){
						res.json({"stocks": portfolio, "wallet":user.wallet, "favoriteStocks":user.favoriteStocks})
					}else{
						res.json(err)
					}
				})
			}else{
				res.json(err)
			}
		})
	},

	getEquity:function(req,res){
		Portfolio.findOne({user:req.params.id,stock:req.params.info},function(err,portfolio){
			if(!err){
				if(portfolio){
					res.json({
						"equity":portfolio.equity,
						"quantity":portfolio.quantity
					})
				}else{
					res.json({message:"zeroOwned"})
				}
			}else{
				res.json(err)
			}
		})
	},

	createBankAccount:function(req,res){
		User.findById({_id:req.body.userId},function(err,user){
			if(!err){
				user.bankAccounts.push(req.body.nickName)
				user.save()
				res.json({message:"success"})
			}else{
				res.json(err)
			}
		})
	},

	deleteBankAccount:(req,res)=>{
		User.findById({_id:req.body.userId},function(err,user){
			if(!err){
				user.bankAccounts.pull(req.body.nickName)
				user.save()
				res.json({message:"success"})
			}else{
				res.json(err)
			}
		})
	},

	depositFunds:(req,res)=>{
		User.findById({_id:req.body.userId},function(err,user){
			if(!err){
				Transaction.create({account:req.body.cardName,transferAmount:req.body.amount},(err,transaction)=>{
					if(!err){
						transaction.user=req.body.userId;
						user.wallet+=req.body.amount;
						user.save()
						transaction.save();
						res.json({message:"success"});
					}else{
						res.json(err)
					}
				})
			}else{
			}
		})
	},

	getTransactions:(req,res)=>{
		Transaction.find({user:req.params.id},(err,transaction)=>{
			if(!err){
				res.json(transaction);
			}else{
				res.json({"err":err})
			}
		})
	},
	addFavorite:(req,res)=>{
		User.findById({_id:req.body.userId},(err,user)=>{
			if(!err){
				let found;
				user.favoriteStocks.forEach(element => {
					if(element.symbol==req.body.symbol){
						found=element;
					} 
				});
				if(found){
					user.favoriteStocks.pull(found);
					user.save();
					res.json({message:"removed"})
				}if(!found){
					user.favoriteStocks.push(req.body)
					user.save();	
					res.json({message:"added"})
				}
			}else{
				res.json({"err":err})
			}
		})
	}
}
